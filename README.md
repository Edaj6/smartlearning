# README #

MVC App med ms identity security. 
Demo på https://jakob.azurewebsites.net/ (login med: usr@mail.dk - P@ssw0rd eller opret egen bruger)

### What is this repository for? ###

* Ikke til produktion, kun til test !!! 
* Del af kursus på smartlearning - udvikling af store systemer
* Viser brug af Azure web services og cosmos db/ms sql server
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Summary of set up
* Database configuration: Tilføj connection string til appsettings, kør add-migration initial fra vs studio command, derefter update-database
* Deployment instructions: tilføj publish profile til startup projekt

### Contribution guidelines ###

* Kommenter gerne hvis der er fejl :-)

### Who do I talk to? ###

* Repo owner/admin - Jakob (smartlearning - udvikling af store systemer)
