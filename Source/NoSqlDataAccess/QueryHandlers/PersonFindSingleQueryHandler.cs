﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Queries;
using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;

namespace NoSqlDataAccess.QueryHandlers
{
    public class PersonFindSingleQueryHandler : IQueryHandler<PersonFindSingleQuery, Task<Person>>
    {
        private readonly Container container;

        public PersonFindSingleQueryHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task<Person> Handle(PersonFindSingleQuery query)
        {
            string birthday = query.CprNumber.Substring(0, 6);
            var sqlQueryText = $"SELECT * FROM c WHERE c.cprNo = '{query.CprNumber}' AND c.birthday = '{birthday}'";

            Console.WriteLine("Running query: {0}\n", sqlQueryText);

            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Person> queryResultSetIterator = this.container.GetItemQueryIterator<Person>(queryDefinition);

            Person person = null;

            if (queryResultSetIterator.HasMoreResults)
            {
                FeedResponse<Person> currentResultSet = await queryResultSetIterator.ReadNextAsync();
              
                foreach (Person p in currentResultSet)
                {
                    person = p;
                }
            }

            return person;
        }
    }
}
