﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Queries;
using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;

namespace NoSqlDataAccess.QueryHandlers
{
    public class PersonSingleQueryHandler : IQueryHandler<PersonSingleQuery, Task<Person>>
    {
        private readonly Container container;

        public PersonSingleQueryHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task<Person> Handle(PersonSingleQuery query)
        {
            ItemResponse<Person> response = await container.ReadItemAsync<Person>(query.Id, new PartitionKey(query.Birthday));

            var person = response.Resource;
            if (person.TestResults == null)
            {
                person.TestResults = Array.Empty<TestResult>();
            }

            return person;
        }
    }
}
