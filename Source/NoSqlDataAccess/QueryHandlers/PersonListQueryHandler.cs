﻿using Domain.Entities;
using Domain.Interfaces;
using Domain.Queries;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NoSqlDataAccess.QueryHandlers
{
    public class PersonListQueryHandler : IQueryHandler<PersonListQuery, Task<Person[]>>
    {
        private readonly Container container;

        public PersonListQueryHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task<Person[]> Handle(PersonListQuery query)
        {
            var sqlQueryText = "SELECT * FROM c"; // WHERE c.LastName = 'Andersen'";

            Console.WriteLine("Running query: {0}\n", sqlQueryText);

            QueryDefinition queryDefinition = new QueryDefinition(sqlQueryText);
            FeedIterator<Person> queryResultSetIterator = this.container.GetItemQueryIterator<Person>(queryDefinition);

            List<Person> persons = new List<Person>();

            while (queryResultSetIterator.HasMoreResults)
            {
                FeedResponse<Person> currentResultSet = await queryResultSetIterator.ReadNextAsync();
                foreach (Person family in currentResultSet)
                {
                    persons.Add(family);
                    Console.WriteLine("\tRead {0}\n", family);
                }
            }

            return persons.ToArray();
        }
    }
}
