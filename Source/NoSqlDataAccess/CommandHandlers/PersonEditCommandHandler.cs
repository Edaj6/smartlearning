﻿using Domain.Commands;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoSqlDataAccess.CommandHandlers
{
    public class PersonEditCommandHandler : ICommandHandler<EditPerson>
    {
        private readonly Container container;

        public PersonEditCommandHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task Handle(EditPerson command)
        {
            command.Birthday = command.CprNo.Substring(0, 6);

            if (command.NewTestResult.Result != null)
            {
                List<TestResult> testResults;

                if (command?.TestResults == null)
                {
                    testResults = new List<TestResult>();
                }
                else
                {
                    testResults = command
                        .TestResults
                        .ToList();
                }

                //add new result     
                testResults.Add(command.NewTestResult);
                command.TestResults = testResults.ToArray();
            }

            await container
                .UpsertItemAsync<Person>(command, new PartitionKey(command.Birthday));
        }
    }
}
