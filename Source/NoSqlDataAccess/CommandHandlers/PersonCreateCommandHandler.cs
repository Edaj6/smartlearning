﻿using Domain.Commands;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;

namespace NoSqlDataAccess.CommandHandlers
{
    public class PersonCreateCommandHandler : ICommandHandler<CreatePerson>
    {
        private readonly Container container;

        public PersonCreateCommandHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task Handle(CreatePerson command)
        {
            if (command.Id == null)
                command.Id = Guid.NewGuid().ToString();

            command.Birthday = command.CprNo.Substring(0, 6);

            await container
                .CreateItemAsync<Person>(command, new PartitionKey(command.Birthday));
        }
    }
}
