﻿using Domain.Commands;
using Domain.Entities;
using Domain.Interfaces;
using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;

namespace NoSqlDataAccess.CommandHandlers
{
    public class PersonDeleteCommandHandler : ICommandHandler<DeletePerson>
    {
        private readonly Container container;

        public PersonDeleteCommandHandler(Container container)
        {
            this.container = container ?? throw new ArgumentNullException(nameof(container));
        }

        public async Task Handle(DeletePerson command)
        {
            await container
                .DeleteItemAsync<Person>(command.Id, new PartitionKey(command.Birthday));

        }
    }
}
