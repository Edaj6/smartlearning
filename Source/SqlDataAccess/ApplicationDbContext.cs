using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace SqlDataAccess
{
    public class ApplicationDbContext : IdentityDbContext
    {
        //for building sql db
        public ApplicationDbContext() : base(GetOptions())
        {
        }

        public ApplicationDbContext(string connectionString) : base(SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options)
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
