﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    public class AppSettings
    {
        public string SqlConnectionString { get; set; }
        public string Secret { get; set; }
    }
}
