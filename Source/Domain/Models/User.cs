﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class User
    {
        public string Username { get; set; }
        public string  Token { get; set; }
    }
}
