﻿using Domain.Entities;
using Domain.Interfaces;
using System.Threading.Tasks;

namespace Domain.Queries
{
    public class PersonSingleQuery : IQuery<Task<Person>>
    {
        public string Id { get; set; }
        public string Birthday { get; set; }
        public string CprNumber { get; set; }
    }
}
