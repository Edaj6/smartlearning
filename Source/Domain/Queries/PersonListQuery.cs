﻿using Domain.Entities;
using Domain.Interfaces;
using System.Threading.Tasks;

namespace Domain.Queries
{
    public class PersonListQuery : IQuery<Task<Person[]>>
    {
    }
}
