﻿using Domain.Interfaces;
using Domain.Models;
using System.Threading.Tasks;

namespace Domain.Queries
{
    public class AuthenticateUserQuery : IQuery<Task<User>>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
