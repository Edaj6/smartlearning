﻿using Domain.Entities;
using Domain.Interfaces;
using System.Threading.Tasks;

namespace Domain.Queries
{
    public class PersonFindSingleQuery : IQuery<Task<Person>>
    {
        public string CprNumber { get; set; }
    }
}
