﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace Domain.Entities
{
    public class Person
    {
        [JsonProperty(PropertyName = "id")]
        public string  Id { get; set; }

        [JsonProperty(PropertyName = "firstName")]
        [DisplayName("Fornavn")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "lastName")]
        [DisplayName("Efternavn")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "cprNo")]
        [DisplayName("Cpr Nummer")]
        public string CprNo { get; set; }

        [JsonProperty(PropertyName = "birthday")]
        [DisplayName("Fødselsdag")]
        public string Birthday { get; set; }

        [JsonProperty(PropertyName = "testResults")]
        public TestResult[] TestResults { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public class TestResult
    {
        [JsonProperty(PropertyName = "testDate")]
        [DisplayName("Test dato")]
        public DateTime TestDate { get; set; }

        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }
    }
}
