﻿namespace Domain.Entities
{
    public class Initiative
    {
        public string  Header { get; set; }
        public string  Details { get; set; }
        public string Period { get; set; }
        public string  TargetGroup { get; set; }        
    }
}
