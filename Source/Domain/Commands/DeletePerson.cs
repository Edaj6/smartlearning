﻿namespace Domain.Commands
{
    public class DeletePerson 
    {
        public string Id { get; set; }
        public string Birthday { get; set; }

    }
}
