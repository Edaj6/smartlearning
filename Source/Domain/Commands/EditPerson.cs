﻿using Domain.Entities;
using System.Linq;

namespace Domain.Commands
{
    public class EditPerson : Person
    {
        public TestResult NewTestResult { get; set; }

        public int NumberOfTestResults => TestResults == null ? 0 : TestResults.Count();
    }
}
