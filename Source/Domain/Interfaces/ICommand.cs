﻿using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ICommand
    {
    }

    public interface ICommandHandler<in TCommand>
    {
        Task Handle(TCommand command);
    }
}
