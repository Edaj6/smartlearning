﻿using Domain.Entities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CosmosDbModifyDataConsoleApp
{
    public class App
    {
        private readonly IConfigurationRoot config;
        private readonly Random random;

        public App(IConfigurationRoot config)
        {
            this.config = config;
            random = new Random();
        }

        public async Task Run(int createNumberOfPersons)
        {
            var cosmosDb = new CosmosDb();
            var container = await cosmosDb.GetCosmosContainerCreateIfNotExists(config);
            
            var list = GetPersonNameList(createNumberOfPersons);

            foreach (var person in list)
            {
                await container.CreateItemAsync<Person>(person, new Microsoft.Azure.Cosmos.PartitionKey(person.Birthday));
            }
        }

        private List<Person> GetPersonNameList(int createNumberofpersons)
        {
            List<Person> personList = new List<Person>();
            NameList nameList;
            JsonSerializer serializer = new JsonSerializer();

            using (StreamReader reader = new StreamReader("names.json"))
            using (JsonReader jreader = new JsonTextReader(reader))
            {
                nameList = serializer.Deserialize<NameList>(jreader);
            }

            var FirstName = new List<string>(nameList.boys);
            FirstName.AddRange( nameList.girls);
            var LastName = new List<string>(nameList.last);

            string firstName;
            string lastName;
            string cpr;
            TestResult[] testresults = new TestResult[] { new TestResult { Result = "Negativ" } };

            Console.WriteLine($"Creating {createNumberofpersons} persons");
            for (int i = 0; i < createNumberofpersons; i++)
            {
                //get random person
                firstName = FirstName[random.Next(FirstName.Count)];
                lastName = LastName[random.Next(LastName.Count)];
                cpr = GetRandomCpr();


                personList.Add(new Person {Id = Guid.NewGuid().ToString(), FirstName = firstName, LastName = lastName, CprNo = cpr, Birthday = cpr.Substring(0,6), TestResults = testresults });
            }

            return personList;
        }

        private string GetRandomCpr()
        {
            StringBuilder cpr = new StringBuilder();
            cpr.Append(random.Next(10, 28));    //date            
            cpr.Append("0" + random.Next(1, 9));      //month
            cpr.Append(random.Next(50, 99));    //year
            cpr.Append("-");
            cpr.Append(random.Next(1000, 9999));
            return cpr.ToString();
        }
    }

    class NameList
    {
        public string[] boys { get; set; }
        public string[] girls { get; set; }
        public string[] last { get; set; }

        public NameList()
        {
            boys = new string[] { };
            girls = new string[] { };
            last = new string[] { };
        }
    }
}

