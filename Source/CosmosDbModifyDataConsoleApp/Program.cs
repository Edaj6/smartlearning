﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace CosmosDbModifyDataConsoleApp
{
    class Program
    {
        public static IConfigurationRoot configuration;
       
        static void Main(string[] args)
        {
            Console.WriteLine("Create some data Y/N ?");
            var userinput = Console.ReadKey();

            if (!userinput.KeyChar.ToString().ToUpper().Equals("Y"))
            {
                Console.WriteLine("Data not created");
                return;
            }

            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            var service = serviceProvider.GetService<App>();

            service.Run(createNumberOfPersons: 9000).Wait();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Build configuration
            configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();

            // Add access to generic IConfigurationRoot
            serviceCollection.AddSingleton<IConfigurationRoot>(configuration);

            // Add app
            serviceCollection.AddTransient<App>();
        }
    }
}
