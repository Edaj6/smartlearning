﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Cosmos = Microsoft.Azure.Cosmos;

namespace CosmosDbModifyDataConsoleApp
{
    public class CosmosDb
    {
        public async Task<Cosmos.Container> GetCosmosContainerCreateIfNotExists(IConfigurationRoot configuration)
        {
            string databaseName = configuration.GetSection("CosmosDb:DatabaseName").Value;
            string containerName = configuration.GetSection("CosmosDb:ContainerName").Value;
            string account = configuration.GetSection("CosmosDb:Account").Value;
            string key = configuration.GetSection("CosmosDb:Key").Value;
            string partionKeyPath = configuration.GetSection("CosmosDb:PartionKeyPath").Value;
            var client = new Cosmos.CosmosClient(account, key);

            Cosmos.Database database = await client.CreateDatabaseIfNotExistsAsync(databaseName);
            
            //create container with unique key cprno 
            var builder = await database.DefineContainer(name: containerName, partitionKeyPath: partionKeyPath)
                .WithUniqueKey()
                .Path("/cprNo")
                .Attach()
                .CreateIfNotExistsAsync();

            return builder.Container;
        }
    }
}
