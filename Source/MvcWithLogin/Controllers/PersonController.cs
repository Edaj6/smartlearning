﻿using Domain.Commands;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PersonController : Controller
    {
        private readonly IQueryHandler<PersonListQuery, Task<Person[]>> queryHandler;
        private readonly IQueryHandler<PersonSingleQuery, Task<Person>> querySingleHandler;
        private readonly IQueryHandler<PersonFindSingleQuery, Task<Person>> queryFindSingleHandler;
        private readonly ICommandHandler<CreatePerson> createCommandHandler;
        private readonly ICommandHandler<EditPerson> editCommandHandler;
        private readonly ICommandHandler<DeletePerson> deleteCommandHandler;

        public PersonController(
            IQueryHandler<PersonListQuery, Task<Person[]>> queryHandler,
            IQueryHandler<PersonSingleQuery, Task<Person>> querySingleHandler,
            IQueryHandler<PersonFindSingleQuery, Task<Person>> queryFindSingleHandler,
            ICommandHandler<CreatePerson> createCommandHandler,
            ICommandHandler<EditPerson> editCommandHandler,
            ICommandHandler<DeletePerson> deleteCommandHandler
            )
        {
            this.queryHandler = queryHandler ??
                throw new ArgumentNullException(nameof(queryHandler));
            this.querySingleHandler = querySingleHandler ??
                throw new ArgumentNullException(nameof(querySingleHandler));
            this.queryFindSingleHandler = queryFindSingleHandler ??
                throw new ArgumentNullException(nameof(queryFindSingleHandler));
            this.editCommandHandler = editCommandHandler
                ?? throw new ArgumentNullException(nameof(editCommandHandler));
            this.deleteCommandHandler = deleteCommandHandler ??
                throw new ArgumentNullException(nameof(deleteCommandHandler));
            this.createCommandHandler = createCommandHandler ??
                throw new ArgumentNullException(nameof(createCommandHandler));
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            ViewData["ShowRecords"] = 15;
            var persons = await queryHandler.Handle(new PersonListQuery());
            return View(persons);
        }

        [HttpGet("IndexShowAll")]
        public async Task<IActionResult> IndexShowAll()
        {
            ViewData["ShowRecords"] = 100000;
            var persons = await queryHandler.Handle(new PersonListQuery());
            return View("Index", persons);
        }

        [HttpPost]
        public async Task<IActionResult> Index(string cprNumber)
        {
            //todo add text onbad input and person not found
            if (string.IsNullOrEmpty(cprNumber) || cprNumber.Length < 10)
            {
                return View("Index", new Person[0]);
            }

            var person = await queryFindSingleHandler.Handle(new PersonFindSingleQuery { CprNumber = cprNumber });

            if (person == null)
            {
                return View("Index", new Person[0]);
            }
            else
            {
                return View("Details", person);
            }
        }        

        [HttpPost("Edit")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditAsync([FromForm] EditPerson person)
        {
            if (person.Id == null)
            {
                return BadRequest();
            }

            await editCommandHandler.Handle(person);

            //TODO, reset field not working 
            person.NewTestResult.Result = null;

            return View(person);
        }

        [HttpGet("Edit")]
        public async Task<IActionResult> EditAsync(string id, string birthday)
        {
            if (id == null)
            {
                return BadRequest();
            }

            //TODO handle cpr/birthday change
            var person = await querySingleHandler.Handle(new PersonSingleQuery { Id = id, Birthday = birthday });

            if (person == null)
            {
                return NotFound();
            }

            //TODO use automapper 
            var editPerson = new EditPerson { Id = person.Id, FirstName = person.FirstName, LastName = person.LastName, CprNo = person.CprNo, TestResults = person.TestResults };
            editPerson.NewTestResult = new TestResult { TestDate = DateTime.Now };

            return View(editPerson);
        }

        [HttpGet("Details")]
        public async Task<IActionResult> DetailsAsync(string id, string birthday)
        {
            var person = await querySingleHandler.Handle(new PersonSingleQuery { Id = id, Birthday = birthday });

            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        [HttpGet("Create")]
        public IActionResult CreateAsync()
        {
            var person = new CreatePerson();
            return View(person);
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAsync([FromForm] CreatePerson person)
        {
            await createCommandHandler.Handle(person);
            return View(person);
        }

        [HttpGet("Delete")]
        public async Task<IActionResult> DeleteAsync(string id, string birthday)
        {
            var person = await querySingleHandler.Handle(new PersonSingleQuery { Id = id, Birthday = birthday});
            return View(person);
        }

        [HttpGet("DoDelete")]        
        public async Task<IActionResult> DoDeleteAsync(string id, string birthday)
        {
            await deleteCommandHandler.Handle(new DeletePerson { Id = id, Birthday = birthday});
            return RedirectToAction("Index");
        }
    }
}

