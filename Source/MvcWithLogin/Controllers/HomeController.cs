﻿using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MvcWithLogin.Models;
using System.Collections.Generic;
using System.Diagnostics;

namespace MvcWithLogin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Initiatives()
        {
            var inititatives = new List<Initiative>
            {
                new Initiative
                {
                    Header = "Netværk om familiebehandling og støtte til forældre til anbragte",
                    Details = "Kommunen danner rammen om to netværk, der skal styrke kommunens udviklingsarbejde. Det ene netværk har fokus på styrket familiebehandling til de mest udsatte familier. Det andet har fokus på støtte til forældre til anbragte.",
                    Period = "2020 – 2022",
                    TargetGroup = "Målgruppen er kommunale ledere, som er ansvarlige for udviklingen og driften af kommunens arbejde på det specialiserede børneområde. Det kunne fx være ledere af myndighed, familiebehandling, forebyggelse, anbringelse og PPR. Det kan også være relevant, at centrale udviklings –og nøglemedarbejdere, der fx driver en udviklingsproces, deltager. Der kan deltage to til tre fra hver kommune i hvert netværk. Antallet pr. kommune afhænger af, hvor stor interessen for det konkrete netværk er."
                },
                new Initiative
                {
                    Header = "Undersøgelse om vold i nære relationer",
                    Details = "Kommunen udarbejder hvert andet år undersøgelser om vold i nære relationer, bl.a. med fokus på karakteren af den vold, som voksne og børn udsættes for.",
                    Period = "2020 – 2024",
                    TargetGroup = "Kvinder, mænd og børn, som er udsat for vold i nære relationer, og som har ophold på kvindekrisecenter, mandecenter, mandekrisecenter, eller ambulant tilbud målrettet voldsudsatte personer."
                },
                new Initiative
                {
                    Header = "Ny kvalitetsmodel i frivilligcentrene",
                    Details = "En ny kvalitetsmodel i landets frivilligcentre skal udgøre en fælles standard og samtidig understøtte mangfoldigheden blandt frivilligcentrene.",
                    Period = "2018 – 2021",
                    TargetGroup = "Frivilligcentrene"
                },
                new Initiative
                {
                    Header = "Behov for bedre rødvin på plejehjem",
                    Details = "Stor andel af de ældre foretrækker Amarone eller Kult Pinot Noir fra Napa Valley. Vi burde kunne gøre det bedre",
                    Period = "2010-2040",
                    TargetGroup = "Plejehjem"
                }
            };

            return View(inititatives);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
