using Autofac;
using Domain.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SqlDataAccess;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Cosmos = Microsoft.Azure.Cosmos;

namespace MvcWithLogin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();
            services.AddRazorPages();

            //not working ?
            services.AddAntiforgery(options =>
            {
                // Set Cookie properties using CookieBuilder properties†.
                options.FormFieldName = "AntiforgeryFieldname";
                options.HeaderName = "X-CSRF-TOKEN-HEADERNAME";
                options.SuppressXFrameOptionsHeader = false;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            AddSecurityHeaders(app);

            app.UseHttpsRedirection();

            //cache for 30 days
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = context =>
                context.Context.Response.Headers.Add("Cache-Control", "public, max-age=2592000")
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            var appServices = app.ApplicationServices;
            CreateRoles(appServices).Wait();
        }

        private void AddSecurityHeaders(IApplicationBuilder app)
        {
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Xss-Protection", "1");
                context.Response.Headers.Add("X-Frame-Options", "DENY");
                context.Response.Headers.Add("Referrer-Policy", "no-referrer");
                context.Response.Headers.Add("X-Content-Type-Options", "nosniff");
                context.Response.Headers.Add(
                "Content-Security-Policy",
                "default-src 'self'; " +
                "img-src 'self'; " +
                "font-src 'self'; " +
                "style-src 'self'; " +
                "script-src 'self'; " +
                "frame-src 'self';" +
                "connect-src 'self';");

                context.Response.Headers.Add(
                "Permissions-Policy",
                "accelerometer=(); " +
                "camera=(); " +
                "geolocation=('self'); " +
                "gyroscope=(); " +
                "magnetometer=(); " +
                "microphone=(); " +
                "payment=();" +
                "usb=()");
                await next();
            });
        }

        //Permissions-Policy: geolocation=(self "https://example.com"), microphone=()

        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            string[] roleNames = { "Admin", "Manager", "User" };

            foreach (var roleName in roleNames)
            {
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
        }

        //setup autofac dependency injection
        public void ConfigureContainer(ContainerBuilder builder)
        {
            Assembly domainAssembly = typeof(Domain.Entities.Person).Assembly;            
            Assembly noSqlDataAccessAssembly = typeof(NoSqlDataAccess.QueryHandlers.PersonListQueryHandler).Assembly;
            builder.Register(r => Configuration);

            //register ms sql, for ms identity
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            builder.Register(c => new ApplicationDbContext(connectionString)).InstancePerLifetimeScope();

            //register cosmos no sql           
            builder.Register(c => GetCosmosContainer()).AsSelf().InstancePerLifetimeScope();

            //register query and command handlers
            builder.RegisterAssemblyTypes(domainAssembly).AsClosedTypesOf(typeof(IQueryHandler<,>));            
            builder.RegisterAssemblyTypes(noSqlDataAccessAssembly).AsClosedTypesOf(typeof(IQueryHandler<,>));
            builder.RegisterAssemblyTypes(noSqlDataAccessAssembly).AsClosedTypesOf(typeof(ICommandHandler<>));
        }

        private Cosmos.Container GetCosmosContainer()
        {
            string databaseName = Configuration.GetSection("CosmosDb:DatabaseName").Value;
            string containerName = Configuration.GetSection("CosmosDb:ContainerName").Value;
            string account = Configuration.GetSection("CosmosDb:Account").Value;
            string key = Configuration.GetSection("CosmosDb:Key").Value;
            
            var client = new Cosmos.CosmosClient(account, key);
            var db = client.GetDatabase(databaseName);
            return db.GetContainer(containerName);
        }
    }
}
