﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Interfaces;
using Domain.Models;
using Domain.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace MvcWithLogin.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IQueryHandler<AuthenticateUserQuery, Task<User>> queryHandler;

        public UserController(
            SignInManager<IdentityUser> signInManager,
             IQueryHandler<AuthenticateUserQuery, Task<User>> queryHandler)
        {
            this.signInManager = signInManager ?? 
                throw new ArgumentNullException(nameof(signInManager));
            this.queryHandler = queryHandler ?? 
                throw new ArgumentNullException(nameof(queryHandler));
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthenticateUserQuery query)
        {
            var user = queryHandler.Handle(query);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

    }
}
