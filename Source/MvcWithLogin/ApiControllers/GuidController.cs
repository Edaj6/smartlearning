﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Web.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GuidController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
